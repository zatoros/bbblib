﻿#include "GPIO/BBBGpio.h"
#include "Util/BBBUtil.h"

#include <stdio.h>

int main()
{
	struct BBBGpio ledPin;
	ledPin.number = BBB_P9_12;
	ledPin.direction = BBB_DIR_OUT;
	ledPin.value = BBB_VALUE_HIGH;
	if (!BBBGpioInitialize(&ledPin))
	{
		printf("Failed to initialize ledPin");
	}

	for (int index = 0; index < 10; ++index)
	{
		BBBGpioSetHigh(&ledPin);
		BBBDelay(1);
		BBBGpioSetLow(&ledPin);
		BBBDelay(1);
	}

	if (!BBBGpioFree(&ledPin))
	{
		printf("Failed to free ledPin");
	}
	return 0;
}

#include "BBBGpioTypes.h"

int BBBGpioInitialize(const struct BBBGpio * bbbGpio);
int BBBGpioFree(const struct BBBGpio * bbbGpio);

int BBBGpioSetHigh(struct BBBGpio * bbbGpio);
int BBBGpioSetLow(struct BBBGpio * bbbGpio);
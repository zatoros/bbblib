enum BBBGpioPin
{
	BBB_P9_11 = 30,
	BBB_P9_12 = 60,
	BBB_P9_13 = 31,
	BBB_P9_14 = 50,
	BBB_P9_15 = 48,
	BBB_P9_16 = 51,
	BBB_P9_17 = 5,
	BBB_P9_18 = 4,
	BBB_P9_21 = 3,
	BBB_P9_22 = 2,
	BBB_P9_23 = 49,
	BBB_P9_24 = 15,
	BBB_P9_25 = 117,
	BBB_P9_26 = 14,
	BBB_P9_27 = 115,
	BBB_P9_28 = 113,
	BBB_P9_29 = 111,
	BBB_P9_30 = 112,
	BBB_P9_31 = 110,
	BBB_P9_41 = 20,
	BBB_P9_42 = 7,
	BBB_P8_3 = 38,
	BBB_P8_4 = 39,
	BBB_P8_5 = 34,
	BBB_P8_6 = 35,
	BBB_P8_7 = 66,
	BBB_P8_8 = 67,
	BBB_P8_9 = 69,
	BBB_P8_10 = 68,
	BBB_P8_11 = 45,
	BBB_P8_12 = 44,
	BBB_P8_13 = 23,
	BBB_P8_14 = 26,
	BBB_P8_15 = 47,
	BBB_P8_16 = 46,
	BBB_P8_17 = 27,
	BBB_P8_18 = 65,
	BBB_P8_19 = 22,
	BBB_P8_20 = 63,
	BBB_P8_21 = 62,
	BBB_P8_22 = 37,
	BBB_P8_23 = 36,
	BBB_P8_24 = 33,
	BBB_P8_25 = 32,
	BBB_P8_26 = 61,
	BBB_P8_27 = 86,
	BBB_P8_28 = 88,
	BBB_P8_29 = 87,
	BBB_P8_30 = 89,
	BBB_P8_31 = 10,
	BBB_P8_32 = 11,
	BBB_P8_33 = 9,
	BBB_P8_34 = 81,
	BBB_P8_35 = 8,
	BBB_P8_36 = 80,
	BBB_P8_37 = 78,
	BBB_P8_38 = 79,
	BBB_P8_39 = 76,
	BBB_P8_40 = 77,
	BBB_P8_41 = 74,
	BBB_P8_42 = 75,
	BBB_P8_43 = 72,
	BBB_P8_44 = 73,
	BBB_P8_45 = 70,
	BBB_P8_46 = 71
};

enum BBBGpioDirection
{
	BBB_DIR_IN,
	BBB_DIR_OUT
};

enum BBBGpioValue
{
	BBB_VALUE_LOW = 0,
	BBB_VALUE_HIGH = 1
};

struct BBBGpio
{
	enum BBBGpioPin number;
	enum BBBGpioDirection direction;
	enum BBBGpioValue value;
};
#include "GPIO/BBBGpio.h"
#include "Util/BBBUtil.h"

#include <stdio.h>
#include <string.h>

#define GPIO_BASE_PATH "/sys/class/gpio"
#define GPIO_EXPORT_PATH  (GPIO_BASE_PATH"/export")
#define GPIO_UNEXPORT_PATH  (GPIO_BASE_PATH"/unexport")

#define GPIO_PATH_BUFFER_SIZE 40

static const char * GPIO_DIRECTION_PATH = "direction";
static const char * GPIO_NUMBER_PATH = "gpio";
static const char * GPIO_VALUE_PATH = "value";

static void getGpioPath(const enum BBBGpioPin pin, const char * suffix, char * path)
{
	sprintf(path, "%s/gpio%d/%s", (GPIO_BASE_PATH), pin, suffix);
}

static void getGpioDirectionPath(const enum BBBGpioPin pin, char * path)
{
	getGpioPath(pin, GPIO_DIRECTION_PATH, path);
}

static void getGpioValuePath(const enum BBBGpioPin pin, char * path)
{
	getGpioPath(pin, GPIO_VALUE_PATH, path);
}

static FILE * getGpioFile(const enum BBBGpioPin pin, void(*pathGetter)(const enum BBBGpioPin pin, char * path))
{
	char pathBuffer[GPIO_PATH_BUFFER_SIZE];
	pathGetter(pin, pathBuffer);
	FILE * fValue = fopen(pathBuffer, "w");
	if (!fValue)
		return NULL;
	return fValue;
}

static FILE * getGpioDirectionFile(const enum BBBGpioPin pin)
{
	return getGpioFile(pin, getGpioDirectionPath);
}

static FILE * getGpioValueFile(const enum BBBGpioPin pin)
{
	return getGpioFile(pin, getGpioValuePath);
}

static int exportGpio(const enum BBBGpioPin pin)
{
	FILE * fExport = fopen(GPIO_EXPORT_PATH, "w");
	if (!fExport)
		return 0;
	fprintf(fExport, "%d", pin);
	fclose(fExport);
	return 1;
}

static int unexportGpio(const enum BBBGpioPin pin)
{
	FILE * fExport = fopen(GPIO_UNEXPORT_PATH, "w");
	if (!fExport)
		return 0;
	fprintf(fExport, "%d", pin);
	fclose(fExport);
	return 1;
}

static int setGpioDirection(const enum BBBGpioPin pin, const enum BBBGpioDirection direction)
{
	FILE * fDirection = getGpioDirectionFile(pin);
	if (!fDirection)
		return 0;
	fprintf(fDirection, "%s", (direction == BBB_DIR_IN) ? "in" : "out");
	fclose(fDirection);
	return 1;
}

static int setGpioValue(const enum BBBGpioPin pin, const enum BBBGpioValue value)
{
	FILE * fValue = getGpioValueFile(pin);
	if (!fValue)
		return 0;
	fprintf(fValue, "%d", value);
	fclose(fValue);
	return 1;
}

int BBBGpioInitialize(const struct BBBGpio * bbbGpio)
{
	if (!bbbGpio)
		return 0;

	if (!exportGpio(bbbGpio->number))
		return 0;

	BBBDelay(1);

	if (!setGpioDirection(bbbGpio->number, bbbGpio->direction))
		return 0;

	if (!setGpioValue(bbbGpio->number, bbbGpio->value))
		return 0;

	return 1;
}

int BBBGpioFree(const struct BBBGpio * bbbGpio)
{
	if (!bbbGpio)
		return 0;

	if (!unexportGpio(bbbGpio->number))
		return 0;

	return 1;
}

int BBBGpioSetHigh(struct BBBGpio * bbbGpio)
{
	if (!bbbGpio)
		return 0;

	if (!setGpioValue(bbbGpio->number, BBB_VALUE_HIGH))
		return 0;
	bbbGpio->value = BBB_VALUE_HIGH;
	return 1;
}

int BBBGpioSetLow(struct BBBGpio * bbbGpio)
{
	if (!bbbGpio)
		return 0;

	if (!setGpioValue(bbbGpio->number, BBB_VALUE_LOW))
		return 0;
	bbbGpio->value = BBB_VALUE_HIGH;

	return 1;
}
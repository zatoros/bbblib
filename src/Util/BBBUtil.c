#include "Util/BBBUtil.h"

#ifndef WIN32
  #include <unistd.h>
#else
inline void sleep(int a) { (void)a; }
#endif 

void BBBDelay(int delaySec)
{
	sleep(delaySec);
}